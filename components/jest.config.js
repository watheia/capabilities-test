module.exports = {
  projects: ['<rootDir>/components', '<rootDir>/sites/case-studies', '<rootDir>/sites/hybrid-homepage'],
  displayName: "components",
  preset: "./jest.preset.js",
  transform: {
    "^.+\\.[tj]sx?$": "babel-jest",
  },
  moduleFileExtensions: ["ts", "tsx", "js", "jsx"],
  coverageDirectory: "../../coverage/libs/components",
};
