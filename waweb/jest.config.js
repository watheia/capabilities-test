module.exports = {
  projects: [
    '<rootDir>/apps/pwa',
    '<rootDir>/apps/api',
    '<rootDir>/apps/demo/portal',
  ],
};
